import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

const contactPage = "Page contact";
final Uri _url = Uri.parse('https://google.fr');

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Ink(
                    decoration: const ShapeDecoration(
                      color: Colors.lightBlue,
                      shape: CircleBorder(),
                    ),
                    child: IconButton(
                      icon: const Icon(Icons.contact_page),
                      color: Colors.white,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const PageContact()),
                        );
                      },
                    ),
                  ),
                  TextButton(
                    child: const Text(
                      contactPage,
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                    style:
                        TextButton.styleFrom(backgroundColor: Colors.lightBlue),
                    onPressed: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const PageContact()),
                      )
                    },
                  )
                ]),
          ),
          Stack(alignment: Alignment.centerLeft, children: <Widget>[
            Container(
                margin: const EdgeInsets.only(left: 15),
                child: TextButton(
                  child: const Text(
                    contactPage,
                    style: TextStyle(fontSize: 18, color: Colors.white),
                  ),
                  style:
                      TextButton.styleFrom(backgroundColor: Colors.lightBlue),
                  onPressed: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const PageContact()),
                    )
                  },
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                    margin: const EdgeInsets.only(right: 15),
                    child: Ink(
                      decoration: const ShapeDecoration(
                        color: Colors.lightBlue,
                        shape: CircleBorder(),
                      ),
                      child: IconButton(
                        icon: const Icon(Icons.contact_page),
                        color: Colors.white,
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const PageContact()),
                          );
                        },
                      ),
                    )),
              ],
            ),
          ])
        ],
      )),
    );
  }
}

class PageContact extends StatelessWidget {
  const PageContact({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Page contact'),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(Icons.close))
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                buildCard(),
                buildCard(),
                buildCard(),
                buildCard(),
                buildCard(),
                buildCard(),
                buildCard(),
                buildCard(),
                buildCard(),
                buildCard(),
                buildCard(),
              ],
            )),
      ),
    );
  }

  InkWell buildCard() {
    var nomPrenom = 'Nom Prénom';
    var numero = '01.02.03.04.05';
    return InkWell(
        onTap: () {
          _launchUrl();
        },
        child: Card(
            elevation: 4.0,
            child: Column(
              children: [
                ListTile(
                  title: Text(
                    nomPrenom,
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 18),
                  ),
                  subtitle: Text(
                    numero,
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 14),
                  ),
                ),
              ],
            )));
  }

  void _launchUrl() async {
  if (!await launchUrl(_url)) throw 'Could not launch $_url';
}
}
